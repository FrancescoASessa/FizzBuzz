<?php

declare(strict_types = 1);

namespace App;

use App\Exceptions\ValidationException;
use App\Validators\ValidatorInteger as Validator;

/**
 * A simple FizzBuzz exercise.
 *
 * PHP version 7
 *
 * @author     Francesco Antonio Sessa <hello@francescosessa.com>
 */

class FizzBuzz
{
    /**
     * Input integers array
     * @var array
     * @access protected
     */
    protected $integers;

    /**
     * Output array
     * @var array
     * @access protected
     */
    protected $outputs;

    /**
     * Validate and push integer into integers proprety
     * @param int $integer integer between 1 and 100
     */
    public function setInteger(int $integer): int
    {
        if (Validator::check($integer))
            return $this->integers[] = $integer;
        else
            throw new ValidationException("Validation error", -1);    
    }

    /**
     * Validate and push integers into integers proprety
     * @param int $integer integer between 1 and 100
     */
    public function setIntegers(int ...$integers): array
    {   
        foreach ($integers as $integer)
            $this->setInteger($integer);
        return $this->getIntegers();
    }

    /**
     * Retrieve integers array
     */
    public function getIntegers(): array
    {
        return $this->integers;
    }

    /**
     * Retrieve outputs array
     */
    public function getOutputs(): array
    {
        return $this->outputs;
    }

    /**
     * FizzBuzz core method :)
     * For each integer into integers proprety return
     * If the integer is divisible by 3 also “Fizz”
     * If the integer is divisible by 5 also “Buzz”
     * @param  int $integer integer value
     * @return void populate outputs proprety
     */
    public function generate(int $integer): void
    {   
        if($integer % 3 == 0)
            $output = 'Fizz';
        
        if($integer % 5 == 0)
            $output = 'Buzz';

        if( ($integer % 3 == 0) && ($integer % 5 == 0) )
            $output = 'Fizz Buzz';

        $this->outputs[] = [$integer,$output]; 
    }
}