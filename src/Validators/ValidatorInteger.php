<?php

declare(strict_types = 1);

namespace App\Validators;

final class ValidatorInteger extends Validator
{
    /**
     * Checks whether the integer is between 1 and 100
     * @param  int    $integer
     * @return bool
     */
	public static function check(int $integer): bool
	{
        $check = filter_var(
            $integer,
            FILTER_VALIDATE_INT, 
            [
                'options' => [
                    'min_range' => 1, 
                    'max_range' => 100
                ]
            ]
        );

	    return ( $check ) === false ? false : true;
	}
}