# Fizz Buzz Class

Simple Fizz Buzz Class

### Prerequisites

PHP >= 7

### Installing

```
composer install
```

## Running the tests

```
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/FizzBuzzTest.php
```