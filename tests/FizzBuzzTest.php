<?php

declare(strict_types = 1);

namespace Tests;

use App\FizzBuzz;
use PHPUnit\Framework\TestCase;

final class FizzBuzzTest extends TestCase
{
    public function testInputSet() 
    {
		$obj = new FizzBuzz();

		$obj->setIntegers(12,13,14,15,16,17,18);

		foreach ($obj->getIntegers() as $integer)
			$obj->generate($integer);

		$generated = $obj->getOutputs();

		$expected = [
			[12,'Fizz'],[13,NULL],
			[14,NULL],[15,'Fizz Buzz'],
			[16,NULL],[17,NULL],
			[18,'Fizz']
		];

        $this->assertEquals($expected, $generated);
    }
}